<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('hajactivity.index');
    // return view('welcome');
});*/



Route::get('ativity/index', function () {
	// show haj activity
    return view('hajactivity.index');
});

Route::get('/groupactivity', ['as' => 'group.activity.index','uses' => 'GroupActivityController@index']);


Route::get('/l', function () {
    return view('user.login');
    // return view('welcome');
});


Route::get('/login',  ['as' => 'login','uses' => 'LoginController@showLoginForm'] );
Route::post('/dologin',  ['as' => 'dologin','uses' => 'LoginController@authenticate'] );
Route::get('/',  ['as' => 'home','uses' => 'HomeController@index'] );

Route::auth();

/*



Route::auth();

Route::get('/home', 'HomeController@index');*/


