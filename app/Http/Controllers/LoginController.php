<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //

	public function showLoginForm(Request $request)
	{
		return view('user.login');
	}
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('home');
        }

        else
        {
        	return redirect()->route('login');
        }

    }

}
