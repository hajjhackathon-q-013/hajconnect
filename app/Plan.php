<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'plan';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function activityList()
    {
        return $this->belongsTo('App\ActivityList','activity_list_id','id');
    }


}
