<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampGroup extends Model
{
    //
    protected $table = 'camp_group';


    public function plan()
	{
		return $this->hasMany('App\Plan','group_id','id');
	}

}
