-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 02, 2018 at 02:57 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hajConnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `all_stoning_schedule`
--

CREATE TABLE `all_stoning_schedule` (
  `id` int(11) NOT NULL,
  `officePublicId` int(11) DEFAULT NULL,
  `est_id` int(11) DEFAULT NULL,
  `gate` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `campLabel` varchar(45) DEFAULT NULL,
  `dispatching_date` date DEFAULT NULL,
  `dispatching_time` time DEFAULT NULL,
  `pilgrims` int(11) DEFAULT NULL,
  `stoning_day` date DEFAULT NULL,
  `stoning_time` time DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `est_table`
--

CREATE TABLE `est_table` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(45) DEFAULT NULL,
  `name_en` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paths_table`
--

CREATE TABLE `paths_table` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(45) DEFAULT NULL,
  `name_en` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `day` int(11) DEFAULT NULL,
  `est_id` int(11) DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `day`, `est_id`, `event_time`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 9, 1, '19:30:00', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `all_stoning_schedule`
--
ALTER TABLE `all_stoning_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `est_table`
--
ALTER TABLE `est_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paths_table`
--
ALTER TABLE `paths_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `all_stoning_schedule`
--
ALTER TABLE `all_stoning_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `est_table`
--
ALTER TABLE `est_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `paths_table`
--
ALTER TABLE `paths_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
