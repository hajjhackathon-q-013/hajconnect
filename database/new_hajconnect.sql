-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: hajconnect
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_list`
--

DROP TABLE IF EXISTS `activity_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ar` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `isdeleted` int(11) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_list`
--

LOCK TABLES `activity_list` WRITE;
/*!40000 ALTER TABLE `activity_list` DISABLE KEYS */;
INSERT INTO `activity_list` VALUES (1,'الى عرفة',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(2,'الى مزدلفة',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(3,'الى منى',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(4,'الى رمى جمرة العقبة',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(5,'الى طواف الإفاضة',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(6,'الى رمى الجمرات يوم 11',NULL,NULL,0,NULL,0,'2018-08-02 15:05:18',NULL),(7,'الى رمى الجمرات يوم 12',NULL,NULL,0,NULL,0,'2018-08-02 15:07:50',NULL),(8,'الى رمى الجمرات يوم 13',NULL,NULL,0,NULL,0,'2018-08-02 15:07:50',NULL),(9,'الى طواف الوداع',NULL,NULL,0,NULL,0,'2018-08-02 15:07:50',NULL);
/*!40000 ALTER TABLE `activity_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camp_group`
--

DROP TABLE IF EXISTS `camp_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camp_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `camp_list_id` varchar(45) DEFAULT NULL,
  `est_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camp_group`
--

LOCK TABLES `camp_group` WRITE;
/*!40000 ALTER TABLE `camp_group` DISABLE KEYS */;
INSERT INTO `camp_group` VALUES (1,'1',7,'قروب A',0,NULL,0,'2018-08-02 15:26:02',NULL),(2,'1',7,'قروب B',0,NULL,0,'2018-08-02 15:26:02',NULL),(3,'2',4,'قروب A-A',0,NULL,0,'2018-08-02 15:26:56',NULL),(4,'2',4,'قروب B-B',0,NULL,0,'2018-08-02 15:26:56',NULL);
/*!40000 ALTER TABLE `camp_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camp_list`
--

DROP TABLE IF EXISTS `camp_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camp_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `isdeleted` int(11) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camp_list`
--

LOCK TABLES `camp_list` WRITE;
/*!40000 ALTER TABLE `camp_list` DISABLE KEYS */;
INSERT INTO `camp_list` VALUES (1,'حملة الإحسان',NULL,0,NULL,0,'2018-08-02 15:25:24',NULL),(2,'حملة الخير',NULL,0,NULL,0,'2018-08-02 15:25:24',NULL);
/*!40000 ALTER TABLE `camp_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `est_table`
--

DROP TABLE IF EXISTS `est_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `est_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ar` varchar(45) DEFAULT NULL,
  `name_en` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `est_table`
--

LOCK TABLES `est_table` WRITE;
/*!40000 ALTER TABLE `est_table` DISABLE KEYS */;
INSERT INTO `est_table` VALUES (1,'جنوب شرق اسيا','South-east Asia'),(2,'جنوب اسيا','South Asia'),(3,'افريقيا غير العربية','non-Arab Africa'),(4,'الدول العربية','Arab Countires'),(5,'تركيا','Turkey'),(6,'ايران','Iran'),(7,'حجاج الداخل','Internal pilgrims');
/*!40000 ALTER TABLE `est_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan`
--

DROP TABLE IF EXISTS `plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_list_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `event_date` varchar(45) DEFAULT NULL,
  `event_time` time DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan`
--

LOCK TABLES `plan` WRITE;
/*!40000 ALTER TABLE `plan` DISABLE KEYS */;
INSERT INTO `plan` VALUES (1,1,1,0,'9','06:00:00',NULL,NULL,NULL),(2,2,1,1,'9','19:00:00',NULL,NULL,NULL),(3,3,1,2,'10','13:00:00',NULL,NULL,NULL),(4,4,1,3,'10','05:00:00',NULL,NULL,NULL),(5,5,1,4,'10','07:00:00',NULL,NULL,NULL),(6,6,1,5,'11','14:00:00',NULL,NULL,NULL),(7,7,1,6,'12','14:00:00',NULL,NULL,NULL),(8,8,1,7,'13','14:00:00',NULL,NULL,NULL),(9,9,1,8,'13','16:00:00',NULL,NULL,NULL),(10,1,2,0,'9','06:00:00',NULL,NULL,NULL),(11,2,2,1,'9','21:00:00',NULL,NULL,NULL),(12,3,2,2,'10','15:00:00',NULL,NULL,NULL),(13,4,2,3,'10','07:00:00',NULL,NULL,NULL),(14,5,2,4,'10','09:00:00',NULL,NULL,NULL),(15,6,2,5,'11','16:00:00',NULL,NULL,NULL),(16,7,2,6,'12','16:00:00',NULL,NULL,NULL),(17,8,2,7,'13','16:00:00',NULL,NULL,NULL),(18,9,2,8,'13','18:00:00',NULL,NULL,NULL);
/*!40000 ALTER TABLE `plan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-02 21:49:54
