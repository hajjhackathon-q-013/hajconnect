<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material-kit/assets/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('material-kit/assets/img/favicon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		Haj Connect
	</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<!-- CSS Files -->
	<link href="{{ asset('material-kit/assets/css/material-kit.css?v=2.0.4') }}" rel="stylesheet" />
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="{{ asset('material-kit/assets/demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="index-page sidebar-collapse">

	<div class="container">
		<br>
		<div class="row">
			<div class="col-lg-12 col-md-12 ml-auto mr-auto">
				<div class="row">
					@foreach(\App\CampGroup::where('isdeleted',0)->get() as $group)
						@if ($group->plan()->count())
							<div class="col-lg-3 col-md-6">
			                    <!-- Tabs with icons on Card -->
			                    <div class="card card-nav-tabs">
			                        <div class="card-header card-header-primary">
			                            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
			                           <div class="col-md-12" style="text-align: center;">
			                           		<h2>{{$group->name}}</h2>
			                           </div>
			                        </div>
			                        <div class="card-body ">
			                            <table class="table table-striped dt-responsive">
			                            	<tr>
			                            		<th style="text-align: center;">
			                            			<i class="material-icons">transfer_within_a_station</i>
			                            			to Mina
			                            		</th>
			                            	</tr>
			                            	<tr>
			                            		<td style="text-align: center;">
			                            			<i class="material-icons">departure_board</i>
			                            			08:00AM
			                            		</td>
			                            	</tr>
			                            </table>
			                        </div>
			                    <!-- End Tabs with icons on Card -->
			               	 	</div>
							</div>
						@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
	
	<!--   Core JS Files   -->
	<script src="{{ asset('material-kit/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/plugins/moment.min.js') }}"></script>
	<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
	<script src="{{ asset('material-kit/assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="{{ asset('material-kit/assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
	<!--	Plugin for Sharrre btn -->
	<script src="{{ asset('material-kit/assets/js/plugins/jquery.sharrre.js') }}" type="text/javascript"></script>
	<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
	<script src="{{ asset('material-kit/assets/js/material-kit.js?v=2.0.4') }}" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			//init DateTimePickers
			materialKit.initFormExtendedDatetimepickers();

			// Sliders Init
			materialKit.initSliders();
		});
	</script>
</body>

</html>