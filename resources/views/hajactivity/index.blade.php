<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material-kit/assets/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('material-kit/assets/img/favicon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		Haj Connect
	</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<!-- CSS Files -->
	<link href="{{ asset('material-kit/assets/css/material-kit.css?v=2.0.4') }}" rel="stylesheet" />
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="{{ asset('material-kit/assets/demo/demo.css') }}" rel="stylesheet" />
	
	<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
</head>

<body class="index-page sidebar-collapse">
<?php 

	$date = 9;
	$time = "06:00:00";
?>
	<div class="container">
		<br>
		<div class="row">
			<div class="col-lg-12 col-md-12 ml-auto mr-auto">
				<div class="card">
					<form class="form" method="" action="">
						<?php 
							$activity_now = \App\Plan::where('group_id',1)
								->where('event_date',$date)
								->where('event_time',$time)
								->first();
						?>
						@if($activity_now)
						<div class="card-header card-header-primary text-center">
							<h2 class="card-title" >
								{{$activity_now->activityList->name_ar}}
							</h2>
							<h4>
								<div style="color: #fff">
									<a href="http://maps.google.com/?ll={{ $activity_now->home_location }}" style="color: #fff">
										<i class="material-icons">room</i> موقع المخيم
									</a>
								</div>
							</h4>
						</div>
						@endif
						<p class="description text-center"><i class="material-icons" style="font-size: 50px;color: #8527a7">expand_more</i></p>
					</form>
				</div>
				<div class="card">
					<form class="form" method="" action="">
						<br>
						<?php 
							$plan = \App\Plan::where('group_id',1)
								->where('parent',$activity_now->id)
								->first();
						?>
						
						
						<!-- @ foreach(\App\Plan::where('group_id',1)->get() as $plan) -->
						<div>
							<span class="bmd-form-group">
								<div class="input-group">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<div style="text-align: center;">
											<h3>
												<i class="material-icons" style="font-size: 60px">transfer_within_a_station</i>
													إلى {{$plan->activityList->name_ar}}
											</h3>
										</div>
										<div>
											<h2 style="text-align: center;">
												<i class="material-icons">departure_board</i> {{date('h:i A', strtotime($plan->event_time))}}
											</h2>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<a href="http://maps.google.com/?ll={{ $plan->home_location }}"> 
											<div class="input-group-prepend">
												<span class="input-group-text" style="    text-align: center;width: 100%;">
													<!-- <i class="material-icons" style="font-size: 90px">map</i> -->
													<img style="height: 85px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAdJSURBVHhe7Z1XiCxFGEbXnPXFgFkxY0QEFRMKopgzoqCoIAqKGXMW9UHEDCYUc8SIOStmBRVRjA+Ciphz1u+sd5e5u1/XdKie6hnrwAGvdNV0z3RXV/31V+1YJpPJZDKZTCaTyWQymUwmk6nNPnJ3uZaci/+RSct68hf5j/xTfiDvlxfIE+V+cju5vlxazi0HwQJySbng+L9GjNnkRv/9p2V/yQ9S1p/l1/Jj+a58VT4lH5X3ytsCcgw+KymHH87wU0m9f8uJz3pecv4jxcXyW7nc+L88V8jeL71LHilHhoPlxIVxt80uHXPKl2TvF9EVeSJXkkPPFvI32XtxZ8oilpJfyN7juyJN3KxyaFlRfimnXhgvbn6oIjaXHDO1XBc8Qg4lC8n3pbso/FwuLos4XLpyqR3Kpot3wePSXVCvT8tQ7+Uq6cqlduiarquluxDnObIIfthnpCuX2sPkUHCcdBdQJP39bWURi0jGGq5sSn+UK8hOs4vsHVCV9Su5rCxiTfmDdGVTytPb2aaLsMZP0p14GV+UNFFF7CDr/Nht28mmi7HDZ9KdcBUvkiGOl65cSjvXdNG9fVO6k63jbrKIWeQN0pVLKTE0zi05dFnvk+4k6/qdXEUWMa+keXNlU3qITM5l0p1cU4ne8uQVQUj8E+nKppKmi8hEMqp2b6vKkxfqwTCH0qQT0YbJwvR7y0H0eM6QIeh5dS3mdYIcKC5625b86LvKELTdrmwq+W7WkQOBARqTTO5E2pIBIZ8boq13WV3fkq3nBjDWSPUiJWpMCKUI2u17pCubyvNka8Qea9SRrm4ouYHu8OvSlU3hX3JTGZ15ZJlQ+iBkUBgagKV8ip0ERclciUYXm4LQ9C+keM+FZCoiCtyJVeY1BiU9L7rdIbaSXeoO7ygbw8SRq7wL0rXcWIbozXRJLdPVoU5JX46RruIu+Y3s1x3u0k1FJmatACQDrS7OOzh5gYcS77oWHa48d3KQHJYfY8J+Y5SyCReDkKZ2XVmKA+Sw/RgTviJD0eEujKMmfE/27QqTXT6sP8aEPAWhKWDGKGTVu7KDloTvwvcJ74yuZglW9S45hyyC+YqupKmeKmeCuQbiLe7gYZYfJfSkkIzBrKQrO0hpkbaW4xATul26A0dBJrdC0VamELowuUUO8zgsH3MHjJIPSuJwRbAia1DzOk6mFSaf5DWkO2jUZJUUUeAi9pSp3p93y0l48f0u3YGjJsHREKxjdOXaltDOTLwt3YFNpAdzqbxJ0mS8IMkgIZaTqnm4VYZ4TLpybTstwsCJugObSK5riPkkKTw0mQQHt5esPXR1xXLandjDwvIP6cq16TtyGqdId3AT6blVZRvp6orlarKIqit/Y3mhnAYpm+7gJnK3V4VxgasrhuQbhyAC68q17eT4o5dVpTu4iTx1VVlZurpiSIiiCOJbv0pXrk3pTNF0T4OlybFP6EBZFdpxV1cMj5ZF7CVdmbZlOVwhsSOgO8uqMG/v6orh5EjYcKN0Zdo2mBMQ+6Q2lHVgBZWrr4mk4RTtUcJN0MZnljF0k4xv7OIK1bVu9ncbYXHbtZwBe664Mm3LBjvBjXN2kq5gXeeXdXhZuvqaeL0sgiRuV6Ztn5RBuKNdwTqyoL4uD0tXZxNZLlHEa9KVaduTZBDaUr5IV7iqH8m6EGpxdTaRpQqORWWq2dF+qUvjxMqHZRefurQRPina/iJVd5cVVqGJs0muk66CqvaLqoY4Xbo668rLk6ffcbl0Zdr2IVmKWEvT2JekLuQruTrr+oYsgt6XK9O2LOkuBTNnroKqniXrwiaYrs663iwdi0l3/CAs9f6A5aWroKpNdjRgrxNXZ11Plo49pDu+bek4lXp/APlBvHBcRVXkYuuygXR11rVo84FLpDu+bZ+QlYgxMAuGBPoQO+K7unSkyl48TVbiGukqqiLh/LqQm+vqrCMzgC5ZjqhyqvFH5ZuVMLWrqIpccF1iRnyLYlhEot3xbUsuQSjzxcIMlqusrEy6NIX1Hq7uqj4gHamyS56TlVlGusrKym7QTYkV8eXF7UgVvzpb1qJJvisX2xSWFLi6q3qUnAoZjCmyS9DOn5eBjVNchWUkWtuUWBFfN2sZu1tdVrIiay+LvlK6Sst4rWzKLdLVXdW15VQOle7YtmU4UZsmGxfH2EYiVsTXTdtyw7hjY8uaxzsli2Y3kza7pCxbSvchZXTtdlVizOKxrbmjjbTZ7yUzgOdKmkkyMqOyhHQfXMZ9ZVNibC3umgimlZtmuFOeCDJ/ToO1mKTCFoX3o0IMiC+X/KpjJYlv3AEkUBNeJ/GMbD/S/PkjKGxDxB9B4fFsSoyI7x1yKptId2zIqE3PsBJjGuB8OZV+G5y13vQMK4ReeDr5cxAkk/FU0vN6RDLOYWedfmMl96ckemcIkzU9owwpsCQqkNVOfhVLGiZ+SL7gqTA38r9uejKZTCaTyWQymUwmk8lkMplMJpPJZDKZHsbG/gUOJDsBfjRctQAAAABJRU5ErkJggg==">
												</span>
											</div>
										
											<div class="input-group-prepend">
												<span class="input-group-text" style="    text-align: center;width: 100%;">
													<i class="material-icons">place</i>
													موقع المخيم
												</span>
											</div>
										</a>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<a href="http://maps.google.com/?ll={{ $plan->trans_location }}">
											<div class="input-group-prepend">
												<span class="input-group-text" style="    text-align: center;width: 100%;">
													<i class="material-icons" style="font-size: 90px">directions_bus</i>
												</span>
											</div>
											<div class="input-group-prepend">
												<span class="input-group-text" style="    text-align: center;width: 100%;">
													<i class="material-icons">place</i>
													موقع المواصلات
												</span>
											</div>
										</a>
									</div>
								</div>
							</span>

						</div>
						<!-- @ endforeach -->
						<div class="footer text-center">
							<a data-toggle="modal" data-target="#help" class="btn btn-primary btn-link btn-wd btn-lg"> <i class="material-icons">pan_tool</i> Help مساعدة</a>
						</div>

						 <!-- Classic Modal -->
						<div class="modal fade" id="help" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title"> Help مساعدة </h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<i class="material-icons">clear</i>
										</button>
									</div>
									<div class="modal-body" style="text-align: center;">
										<button class="btn btn-primary btn-round">
							                <i class="material-icons">call</i> مسؤول التفويج
							            </button>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-danger btn-link" data-dismiss="modal">اغلاق</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!--   Core JS Files   -->
	<script src="{{ asset('material-kit/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/plugins/moment.min.js') }}"></script>
	<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
	<script src="{{ asset('material-kit/assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<!-- <script src="{{ asset('material-kit/assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script> -->
	<!--	Plugin for Sharrre btn -->
	<script src="{{ asset('material-kit/assets/js/plugins/jquery.sharrre.js') }}" type="text/javascript"></script>
	<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
	<script src="{{ asset('material-kit/assets/js/material-kit.js?v=2.0.4') }}" type="text/javascript"></script>
	<script>

		$(document).ready(function() {
			//init DateTimePickers
			materialKit.initFormExtendedDatetimepickers();

			// Sliders Init
			materialKit.initSliders();
		});

		window.addEventListener('devicemotion', function(event) {
			console.log(event.acceleration.x + ' m/s2');
		});
	</script>
</body>

</html>