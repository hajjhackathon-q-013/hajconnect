<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material-kit/assets/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('material-kit/assets/img/favicon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>
		Haj Connect
	</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<!-- CSS Files -->
	<link href="{{ asset('material-kit/assets/css/material-kit.css?v=2.0.4') }}" rel="stylesheet" />
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="{{ asset('material-kit/assets/demo/demo.css') }}" rel="stylesheet" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" />
	<style>
	    body {
		    font-family: 'Ropa Sans', sans-serif;
		    color: #333;
		    max-width: 640px;
		    margin: 0 auto;
		    position: relative;
	    }

	    #githubLink {
	    	position: absolute;
			right: 0;
			top: 12px;
			color: #2D99FF;
	    }

	    h1 {
	      margin: 10px 0;
	      font-size: 40px;
	    }

	    #loadingMessage {
	      text-align: center;
	      padding: 40px;
	      background-color: #eee;
	    }

	    #canvas {
	      max-height: 200px;
	      width: auto;
	    }

	    /*#output {
	      margin-top: 20px;
	      background: #eee;
	      padding: 10px;
	      padding-bottom: 0;
	    }

	    #output div {
	      padding-bottom: 10px;
	      word-wrap: break-word;
	    }*/

	    #noQRFound {
	      text-align: center;
	    }
  	</style>


</head>

<body class="index-page sidebar-collapse">

	<div class="container">
		<br>
		<div class="row">
			<div class="col-lg-12 col-md-12 ml-auto mr-auto">
				<div class="card">
					<form class="form" method="" action="" style="text-align: center;">
						<div class="card-header card-header-primary text-center">
							<h4 class="card-title" >
								استخدم كود السوار للدخول
							</h4>
							<div>
								<i class="fas fa-qrcode fa-5x"></i>
							</div>
						</div>
						<p class="description text-center"></p>

						<div id="loadingMessage" hidden="">🎥 Unable to access video stream (please make sure you have a webcam enabled)</div>
						<canvas id="canvas" hidden></canvas>
						<div id="output" hidden>
							<div id="outputMessage">لم يتم التعرف على الكود.</div>
							<div hidden><b></b> <span id="outputData"></span></div>
						</div>
						
						<div class="footer text-center">
							<a href="#pablo" class="btn btn-primary btn-link btn-wd btn-lg"> <i class="material-icons">pan_tool</i> Help مساعدة</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!--   Core JS Files   -->
	<script src="{{ asset('material-kit/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('material-kit/assets/js/plugins/moment.min.js') }}"></script>
	<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
	<script src="{{ asset('material-kit/assets/js/plugins/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<!-- <script src="{{ asset('material-kit/assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script> -->
	<!--	Plugin for Sharrre btn -->
	<script src="{{ asset('material-kit/assets/js/plugins/jquery.sharrre.js') }}" type="text/javascript"></script>
	<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
	<script src="{{ asset('material-kit/assets/js/material-kit.js?v=2.0.4') }}" type="text/javascript"></script>
	
	<script src="{{ asset('jsQR-master/docs/jsQR.js') }}"></script>

	<script>

		var video = document.createElement("video");
		var canvasElement = document.getElementById("canvas");
		var canvas = canvasElement.getContext("2d");
		var loadingMessage = document.getElementById("loadingMessage");
		var outputContainer = document.getElementById("output");
		var outputMessage = document.getElementById("outputMessage");
		var outputData = document.getElementById("outputData");

		function drawLine(begin, end, color) {
			canvas.beginPath();
			canvas.moveTo(begin.x, begin.y);
			canvas.lineTo(end.x, end.y);
			canvas.lineWidth = 4;
			canvas.strokeStyle = color;
			canvas.stroke();
		}

		// Use facingMode: environment to attemt to get the front camera on phones
		navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function(stream) {
			video.srcObject = stream;
			video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
			video.play();
			requestAnimationFrame(tick);
		});

		function tick() {
			loadingMessage.innerText = "جاري التحقق من الكاميرا"
			if (video.readyState === video.HAVE_ENOUGH_DATA) {
				loadingMessage.hidden = true;
				canvasElement.hidden = false;
				outputContainer.hidden = false;

				canvasElement.height = video.videoHeight;
				canvasElement.width = video.videoWidth;
				canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
				var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
				var code = jsQR(imageData.data, imageData.width, imageData.height);
				if (code) {
					drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
					drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
					drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
					drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
					outputMessage.hidden = true;
					outputData.parentElement.hidden = false;
					outputData.innerHTML = "<i class='material-icons' style='font-size:3px;color:green;'>check_circle_outline</i>";
					return exit;

					alert(code.data);
				} else {
					outputMessage.hidden = false;
					outputData.parentElement.hidden = true;
				}
			}
			requestAnimationFrame(tick);
		}
	</script>
	</script>
</body>

</html>